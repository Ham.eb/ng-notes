import { ElementRef, Renderer2 } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'unique-note-component',
  templateUrl: './unique-note.component.html',
  styleUrls: ['./unique-note.component.css'],
})
export class UniqueNoteComponent implements OnInit {
  showButton: boolean = false;
  fadeText: boolean = true;

  constructor(private renderer: Renderer2) {}

  ngOnInit() {
    let textLength = document.getElementById('notecontent').textContent.length;
    if (textLength < 90) {
      this.fadeText = false;
    }
  }
}
