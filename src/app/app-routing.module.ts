import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UniqueNoteComponent } from './components/unique-note-component/unique-note.component';
import { AddNotesScreenComponent } from './screens/add-notes-screen/add-notes-screen.component';
import { AllNotesComponent } from './screens/all-notes-screen/all-notes.component';

const routes: Routes = [
  { path: '', component: AllNotesComponent },
  { path: ':id', component: AddNotesScreenComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
