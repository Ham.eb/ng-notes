import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNotesScreenComponent } from './add-notes-screen.component';

describe('AddNotesScreenComponent', () => {
  let component: AddNotesScreenComponent;
  let fixture: ComponentFixture<AddNotesScreenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddNotesScreenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNotesScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
